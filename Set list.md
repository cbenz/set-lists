# Ballades

Georgia on my mind (F)

# Medium

Ain't misbehavin' (Eb)
All of me (C)
Bill Bailey won't you please come home (F)
Careless love (F)
Central Avenue Breakdown (Eb)
Do you know what it means to miss New Orleans (C)
Don't get around much anymore (C)
Exactly like you (C)
Fly me to the moon (C, 1er accord Am)
Gee baby ain't I good to you (Eb, 1er accord C)
I can't give you anything but love (G)
I'll see you in my dreams (F, 1er accord Bb)
I'm beginning to see the light (G)
Is you is or is you ain't my baby (Fm)
It's a sin to tell a lie (C)
Oh lady be good (G)
Love (F)
Lullaby of Birdland (Ab, 1er accord Fm)
Mack the knife (C)
Night Train (Ab)
On the sunny side of the street (C)
Please don't talk about me when I'm gone (Eb)
Roses of Picardy (C, 1er accord G)
Satin doll (C)
Seven come eleven (Ab)
Some of these days (F, 1er accord A7)
Stompin' at the Savoy (Db)
Sweet Lorraine (G)
There will never be another you (Eb)
Undecided (Bb)
When you're smiling (Bb / C)
Whispering (Eb) (même grille que Ah si j'avais trois francs cinquante / Groovin' High)

# Rapides

Ain't she sweet (Eb / Bb / C)
Avalon (C)
Bei mir bist du schoen (+ verse) (Am)
Blues my naughty sweetie gives to me (Gm)
Charleston (Bb)
Dinah (Ab / G)
Everybody loves my baby (Ab, 1er accord Fm)
Handful of keys (F)
Hello Dolly (Bb)
Honeysuckle rose (F, 1er accord Gm)
I never knew (F)
I want to be happy (C)
I've found a new baby (+ verse) (Dm)
Indiana (F)
It don't mean a thing (Ab, 1er accord Fm / Bb, 1er accord Gm)
Lulu's back in town (Eb)
Perdido (Bb, 1ers accords Cm/F)
Rosetta (F)
Sheik of araby (+verse) (Bb)
Shine (Eb)
Sweet Georgia Brown (Ab, 1er accord F)
Sweet Sue (G)

# Très rapide

Death Ray Boogie (C)
Boogie-woogie stomp (C)
C jam blues (C)
China boy (F)
Honky Tonk Train Blues (G)
I got rhythm (Bb)
Runnin' wild (Bb)
Swannee river (C)
Take the A Train (C)
Twelfth street rag (Eb)

# Lucas +chant
Hey Hey (blues en E)
I Feel so Good (boogie en G)
Kansas City (D)
In The Evenin' (F)
When Did You Leaved Heaven (Db)
Old Fashioned Love (Eb)
Down By The Riverside (E)
Bill Bailey (G)
Route 66 (Bb)
Just Because (C, même grille que Bill Bailey)
St Louis Blues (C)
Nosey Joe (Blues Bb)

# Instrumentals
Tiny's Tempo (Blues swing en Bb)
Doggin with Doggett (Db)
Stuffy (Db)
Flying Home (Ab)
Sunnyland (Boogie en C)
Things Ain't What They Use To Be (Blues swing en G)