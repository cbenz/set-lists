#!/usr/bin/env bash

if [ ! -f "$1" ]; then
    echo "usage: $0 index.txt"
    exit 1
fi

echo '```'
cat "$1"
echo -e '\n```\n'

sed -e "/^$/d" "$1" | while read line; do
    if [[ "$line" = \#* ]]; then
        echo "$line"
    else
        image_name=$(echo "$line" | sed -e "s/^\(.\+\) (.\+)$/\1/")
        image_path="./images/$image_name.png"
        if [ -f "$image_path" ]; then
            echo "![]($image_path)"
        else
            echo "## $line"
        fi
    fi
    echo -e "\n---\n"
done
