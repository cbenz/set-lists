default: all

all: index.pdf

index.md: index.txt
	./txt_to_md.sh index.txt > index.md

index.html: index.md
	pandoc -f markdown_github --standalone index.md -o index.html

index.pdf: index.html
	wkhtmltopdf --user-style-sheet style.css --load-media-error-handling abort index.html index.pdf

.PHONY: images
images:
	mogrify -background white -alpha remove images/*.png

.PHONY: clean
clean:
	rm -f index.md index.html index.pdf